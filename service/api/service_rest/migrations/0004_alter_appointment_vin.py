# Generated by Django 4.0.3 on 2023-01-26 22:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0003_remove_appointment_car_appointment_vin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='vin',
            field=models.CharField(default='0', max_length=17),
        ),
    ]
