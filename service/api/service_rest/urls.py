from django.urls import path
from .views import api_list_appointment, api_list_techs, api_list_cars, api_show_appointment

urlpatterns = [
    path("appointments/", api_list_appointment, name="appointments"),
    path("appointments/<int:id>", api_show_appointment, name="show_appointment"),
    path("technicians/", api_list_techs, name="techs"),
    path("cars/", api_list_cars, name="cars"),
]
