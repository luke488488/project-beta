from django.shortcuts import render
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment

# Create your views here.
class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_number",
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "owner",
        "date",
        "tech",
        "vin",
        "reason",
        "finished",
    ]
    encoders = {
        "tech": TechnicianEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_appointment(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technum = content["tech"]
            tech = Technician.objects.get(employee_number=technum)
            content["tech"] = tech
        except (Technician.DoesNotExist) as error:
            print(error)
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False,
        )

@require_http_methods(["GET", "POST"])
def api_list_techs(request):
    if request.method == "GET":
        techs = Technician.objects.all()
        return JsonResponse(
            {'techs': techs},
            encoder=TechnicianEncoder,
        )
    else:
        content = json.loads(request.body)
        tech = Technician.objects.create(**content)
        return JsonResponse(
            tech,
            encoder=TechnicianEncoder,
            safe=False,
        )

@require_http_methods(["GET"])
def api_list_cars(request):
    cars = AutomobileVO.objects.all()
    return JsonResponse(
        {'cars': cars},
        encoder=AutomobileVOEncoder,
    )

@require_http_methods(["PUT", "DELETE"])
def api_show_appointment(request, id):
    if request.method == 'PUT':
        content = json.loads(request.body)
        appointment = Appointment.objects.filter(id=id).update(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )
    else:
        count, _ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
