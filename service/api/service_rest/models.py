from django.db import models

class Technician(models.Model):
    name = models.CharField(max_length=100)
    employee_number = models.SmallIntegerField()

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

class Appointment(models.Model):
    owner = models.CharField(max_length=100)
    date = models.DateTimeField()
    reason = models.TextField(null=True, blank=True)
    vin = models.CharField(max_length=17, default='0')
    finished = models.BooleanField(default=False)
    tech = models.ForeignKey(
        Technician,
        related_name = 'appointments',
        null=True,
        on_delete=models.CASCADE
    )

    def __str__(self):
        return self.owner
