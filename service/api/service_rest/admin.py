from django.contrib import admin
from .models import Appointment, AutomobileVO, Technician

# Register your models here.

admin.site.register(Technician)
admin.site.register(AutomobileVO)
admin.site.register(Appointment)
