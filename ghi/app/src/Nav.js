import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className='nav-item dropdown'>
            <a className='nav-link dropdown-toggle' href='#' role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Service
              </a>
              <ul className='dropdown-menu'>
                  <li><NavLink className="dropdown-item" to="/appointments">Appointment list</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/newappointment">Create Appointment</NavLink></li>
                  <li><NavLink className="dropdown-item" to="/newtech">Add Technician</NavLink></li>
              </ul>
          </li>
          <li className='nav-item dropdown'>
            <a className='nav-link dropdown-toggle' href='#' role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales
              </a>
              <ul className='dropdown-menu'>
                  <li><NavLink className="dropdown-item" to="sales/customer">Create a Customer</NavLink></li>
                  <li><NavLink className="dropdown-item" to="sales/employee">Create a Salesman</NavLink></li>
                  <li><NavLink className="dropdown-item" to="sales/new/">Create A Sale</NavLink></li>
                  <li><NavLink className="dropdown-item" to="sales/performance">Salesman Performance</NavLink></li>
              </ul>
          </li>
          <li className='nav-item dropdown'>
            <a className='nav-link dropdown-toggle' href='#' role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
            </a>
              <ul className='dropdown-menu'>
                  <li><NavLink className="dropdown-item" to="sales/automobiles">Inventory List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="sales/automobiles/new/">Add Automobile to Inventory List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="sales/models">Model List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="sales/models/new">Create a Model</NavLink></li>
                  <li><NavLink className="dropdown-item" to="sales/manufacturers">Manufacturer List</NavLink></li>
                  <li><NavLink className="dropdown-item" to="sales/manufacturers/new">Create Manufacturer</NavLink></li>
              </ul>
              </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
