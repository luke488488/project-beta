import React, { useState, useEffect } from 'react'
const TechForm = ({getTechs}) =>{
    const[name, setName] = useState('');
    const[employee_number, setEmployeeNum] = useState('');
    const submit = (event) => {
        event.preventDefault();
        const newE = {
            "name": name,
            "employee_number": employee_number
        }
        const techUrl = "http://localhost:8080/api/technicians/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newE),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(techUrl, fetchConfig)
          .then(() => {
            setName('');
            setEmployeeNum('');
          })
          .catch(e => console.error('error: ', e))
    }
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleEnumChange = (event) => {
        const value = event.target.value;
        setEmployeeNum(value);
    }
    return(
        <div className="row">
            <div className="offset-3 col-6">
              <div className="shadow p-4 mt-4">
                <h1>New Technician</h1>
                <form onSubmit={submit} id="create-shoe-form">
                  <div className="form-floating mb-3">
                    <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                    <label htmlFor="name">Name</label>
                  </div>
                  <div className="form-floating mb-3">
                    <input onChange={handleEnumChange} value={employee_number} placeholder="employee_number" required type="number" name="employee_number" id="employee_number" className="form-control"/>
                    <label htmlFor="employee_number">Employee Number</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
                </form>
              </div>
            </div>
        </div>
    )
}
export default TechForm;
