import { BrowserRouter, Routes, Route } from 'react-router-dom';
import React, { useState, useEffect } from 'react';
import MainPage from './MainPage';
import Nav from './Nav';
import Appointments from './Appointments';
import ApptForm from './NewAppointment';
import TechForm from './NewTech';


import SalesPersonForm from './SalesJS/NewEmployee';
import NewCustomerForm from './SalesJS/NewCustomer';
import SaleForm from './SalesJS/SalesForm';
import SalesList from './SalesJS/SalesHistory';
import SalesSearch from './SalesJS/SalesSearch';

import AutoList from './InventoryJS/AutomobileList';
import ManufacturerList from './InventoryJS/ManufacturerList';
import NewAutoForm from './InventoryJS/NewAutomobile';
import VehicleModelForm from './InventoryJS/NewModel';
import ManufacturerForm from './InventoryJS/NewManufacturer';
import ModelList from './InventoryJS/ModelList';

function App() {
  const [appts, setAppts] = useState([]);
  const [cars, setCars] = useState([]);

  const getAppts = async () => {
    const url = 'http://localhost:8080/api/appointments/';
    const response = await fetch(url);
    if(response.ok){
      const data = await response.json();
      const appts = data.appointments;
      setAppts(appts);
    }
  }
  const getCars = async () => {
    const url = 'http://localhost:8080/api/cars/';
    const response = await fetch(url);
    if(response.ok){
      const data = await response.json();
      const cars = data.cars;
      setCars(cars);
    }
  }


  useEffect (() => {
    getAppts();
    getCars();
  }, [])

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/appointments" element={<Appointments getAppts={getAppts} appts={appts} cars={cars} />} />
          <Route path="/newappointment" element={<ApptForm getAppts={getAppts} />} />
          <Route path="/newtech" element={<TechForm />} />
          <Route path="/sales/new" element={<SaleForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/sales/customer" element={<NewCustomerForm />} />
          <Route path="/sales/employee" element={<SalesPersonForm />} />
          <Route path="/sales/performance" element={<SalesSearch />} />

          <Route path="/sales/automobiles" element={<AutoList />} />
          <Route path="/sales/manufacturers" element={<ManufacturerList />} />
          <Route path="/sales/automobiles/new" element={<NewAutoForm />} />
          <Route path="/sales/models" element={<ModelList />} />
          <Route path="sales/models/new" element ={<VehicleModelForm />} />
          <Route path="/sales/manufacturers/new" element={<ManufacturerForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
