import { NavLink } from 'react-router-dom';
import React, {useState} from 'react'
function Appointments(props) {
    // const[owner, setOwner] = useState('');
    // const[date, setDate] = useState('');
    // const[reason, setReason] = useState('');
    // const[tech, setTech] = useState('');
    // const[vin, setVin] = useState('');

    const [searchvin, setSearchVin] = useState('')
    const handleDelete = async (id) => {
        fetch('http://localhost:8080/api/appointments/'+id, {
            method: 'DELETE'
        }).then(() => {
            return props.getAppts()
        })
        .catch(console.log)
    }
    const handleFinish = async (appt) => {
        const newAppt = {
            'owner': appt.owner,
            'date': appt.date,
            'reason': appt.reason,
            'tech': appt.tech.id,
            'vin': appt.vin,
            'finished': true,
        }
        const apptUrl = "http://localhost:8080/api/appointments/"+appt.id
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(newAppt),
            headers: {
                'Content-Type': 'application/json',
            },
            };
            fetch(apptUrl, fetchConfig)
                .then(() => {
                    return props.getAppts()
                })
                .catch(console.log)
    }

    return(
        <div className="container">
            <div className="input-group mb-3">
            <input type='text' placeholder="Search by vin" onChange={event => {setSearchVin(event.target.value)}} />
            </div>
            <table className="table">
                <thead>
                    <tr>
                        <td>Owner</td>
                        <td>Vin</td>
                        <td>Appt. Time</td>
                        <td>Technician</td>
                        <td>Reason</td>
                        <td>Change Status</td>
                    </tr>
                </thead>
                <tbody>
                {props.appts.filter((val)=>{
                    if(searchvin == ""){
                        return val
                    } else if( val.vin.includes(searchvin)){
                        return val
                    }
                }).map( (appt, key) => {
                    let vip = false;
                    for( let car of props.cars){
                        if(car.vin === appt.vin){
                            vip = true;
                        }
                    }
                    if(vip === false){
                        if(appt.finished === false){
                            return(
                                <tr key={key}>
                                    <td>{appt.owner}</td>
                                    <td>{appt.vin}</td>
                                    <td>{appt.date}</td>
                                    <td>{appt.tech.name}</td>
                                    <td>{appt.reason}</td>
                                    <td>
                                    <button type='button' value={appt.id} onClick ={() => handleDelete(appt.id)}>Delete</button >
                                    <button type='button' value={appt.id} onClick ={() => handleFinish(appt)}>Finished</button >
                                    </td>
                                </tr>
                            );
                        }else{
                            return(
                                <tr key={key}>
                                    <td>{appt.owner}</td>
                                    <td>{appt.vin}</td>
                                    <td>{appt.date}</td>
                                    <td>{appt.tech.name}</td>
                                    <td>{appt.reason}</td>
                                    <td>Completed
                                    <button type='button' value={appt.id} onClick ={() => handleDelete(appt.id)}>Delete</button >
                                    </td>
                                </tr>
                            );
                        }

                    } else {
                        if(appt.finished === false){
                            return(
                                <tr key={key}>
                                    <td><img src="https://www.thefarmersdog.com/digest/wp-content/uploads/2022/03/Pug-cover-2-2000x1226.jpg" width="50px" height="50px" />{appt.owner}</td>
                                    <td>{appt.vin}</td>
                                    <td>{appt.date}</td>
                                    <td>{appt.tech.name}</td>
                                    <td>{appt.reason}</td>
                                    <td>
                                    <button type='button' value={appt.id} onClick ={() => handleDelete(appt.id)}>Delete</button >
                                    <button type='button' value={appt.id} onClick ={() => handleFinish(appt)}>Finished</button >
                                    </td>
                                </tr>
                            );
                            }else{
                                return(
                                    <tr key={key}>
                                        <td><img src="https://www.thefarmersdog.com/digest/wp-content/uploads/2022/03/Pug-cover-2-2000x1226.jpg" width="50px" height="50px" />{appt.owner}</td>
                                        <td>{appt.vin}</td>
                                        <td>{appt.date}</td>
                                        <td>{appt.tech.name}</td>
                                        <td>{appt.reason}</td>
                                        <td>Completed
                                        <button type='button' value={appt.id} onClick ={() => handleDelete(appt.id)}>Delete</button >
                                        </td>
                                    </tr>
                                );
                            }
                    }
                })}
                </tbody>
            </table>
        </div>
    )
}

export default Appointments;
