import React from 'react';


class SalesList extends React.Component {
    constructor(){
        super()
        this.state = {sales: []};
        }

    async componentDidMount(){
        const sales_url = "http://localhost:8090/api/sales/";
        const salesresponse = await fetch(sales_url);
        if (salesresponse.ok){
            const salesdata = await salesresponse.json();
            this.setState({sales: salesdata.sales});
        }
    }
    render() {
    return (
      <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Sales Person</th>
            <th>Vehicle</th>
            <th>Customer</th>
            <th>Purchase Price</th>
          </tr>
        </thead>
        <tbody>
          {this.state.sales.map(sale => {
            return (
              <tr key={ sale.id }>
                <td>{ sale.sales_person.name }</td>
                <td>{ sale.automobile.vin } </td>
                <td>{ sale.customer.name }</td>
                <td>${ sale.purchase_price }.00</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
    }
}
export default SalesList;
