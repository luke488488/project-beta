import React from 'react';

class SalesPersonForm extends React.Component {

    state = {
      name: '',
      employee_number: ''
    };

    handleChange = (e) => {
      const value = e.target.value;
      const name = e.target.name;
      this.setState({[name]: value});
    };

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        const salesUrl = 'http://localhost:8090/api/sales/salesperson/';
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {

          const cleared = {
            name: '',
            employee_number: '',
          };
          this.setState(cleared);
        }
      }

    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create A Salesman</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange= {this.handleChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="Name" className="form-control"/>
                  <label htmlFor="style">Employee Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleChange} value={this.state.employee_number} placeholder="Employee" required type="text" name="employee_number" id="Employee" className="form-control"/>
                  <label htmlFor="style">Employee Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default SalesPersonForm;
