import React from 'react';

class SaleForm extends React.Component {

    state = {
      automobile: '',
      automobiles: [],
      purchase_price: '',
      sales_person: '',
      sales_persons: [],
      customer: '',
      customers: []
    };

    handleChange = (e) => {
      const value = e.target.value;
      const name = e.target.name;
      this.setState({[name]: value});
    };


    handleSubmit = async (e) =>{
        e.preventDefault();
        const data = {...this.state};
        delete data.automobiles;
        delete data.sales_persons;
        delete data.customers;

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {

          const cleared = {
            automobile: '',
            purchase_price: '',
            sales_person: '',
            customer: '',
          };
          this.setState(cleared);
        }
      }

    async componentDidMount(){
        const inventory_url = "http://localhost:8100/api/automobiles/";
        const sales_url = 'http://localhost:8090/api/sales/salesperson/';
        const cust_url = 'http://localhost:8090/api/sales/customer/';
        const response1 = await fetch(inventory_url);
        const response2 = await fetch(sales_url);
        const response3 = await fetch(cust_url);

        if (response1.ok && response2.ok && response3.ok ){
            const cardata  = await response1.json();
            const salesdata = await response2.json();
            const custdata = await response3.json();
            this.setState({automobiles: cardata.autos, sales_persons: salesdata.sales_person, customers: custdata.customer});
        }
    }
    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create Sale Form</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange= {this.handleChange} value={this.state.purchase_price} placeholder="Price" required type="number" name="purchase_price" id="" className="form-control"/>
                  <label htmlFor="style">Purchase Price</label>
                </div>
                <div className="mb-3">
                  <select onChange= {this.handleChange} value={this.state.customer} required name="customer" id="customer" className="form-select">
                    <option value="">Choose a Customer</option>
                    {this.state.customers.map(cust => {
                        return (
                        <option key= {cust.id} value={cust.id}>
                            {cust.name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange= {this.handleChange} value={this.state.sales_person} required name="sales_person" id="salesman" className="form-select">
                    <option value="">Choose A Sales Person</option>
                    {this.state.sales_persons.map(person => {
                        return (
                        <option key= {person.id} value={person.employee_number}>
                            {person.name}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <div className="mb-3">
                  <select onChange= {this.handleChange} value={this.state.automobile} required name="automobile" id="automobile" className="form-select">
                    <option value="">Choose a Vehicle</option>
                    {this.state.automobiles.map(car => {
                        return (
                        <option key= {car.color} value={car.vin}>
                            {car.vin}
                        </option>
                        );
                    })}
                  </select>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default SaleForm;
