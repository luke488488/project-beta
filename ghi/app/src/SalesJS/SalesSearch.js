import React from "react";
import SalesByEmployee from "./SalesSearchTab";


class SalesSearch extends React.Component {
    constructor() {
        super();
        this.state = {
            sales_person: "",
            sales: [],
            people:[],
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleInputChange(event) {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({...this.state, [name]: value})
    }

    async handleSubmit(e){
        e.preventDefault();
        const response = await fetch (`http://localhost:8090/api/sales/salesperson/${this.state.sales_person}/`)
        if (response.ok) {
            const data = await response.json();
            this.setState({ sales: data.sales });
        }
        else {
            console.error(response)
        }
    }

    async componentDidMount() {
        const response = await fetch ("http://localhost:8090/api/sales/salesperson/")
        if (response.ok) {
            const data = await response.json();
            this.setState({ people: data.sales_person });
        }
        else {
            console.error(response)
        }
    }

    render () {
        return (
            <div>
                <h1>Employee Sales</h1>
                <div className="row">
                    <div className="mb-3">
                        <form onSubmit={this.handleSubmit}>
                        <div className="input-group mb-3">
                            <select onChange={this.handleInputChange} value={this.state.person} required name="sales_person" id="salesPerson" className="form-select">
                                <option value="">Choose a Sales Person...</option>
                                {this.state.people.map(person => {
                                return (
                                    <option key= {person.id} value={person.employee_number}>
                                        {person.name}
                                    </option>
                                    )
                                })}
                            </select>
                            <div className="input-group-append">
                            <button className="btn btn-success" type="submit">Search</button>
                            </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div>
                    <SalesByEmployee  sales={this.state.sales}/>
                </div>
            </div>
        )
    }
}

export default SalesSearch;
