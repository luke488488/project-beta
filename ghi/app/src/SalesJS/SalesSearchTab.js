import React from "react";

function SalesByEmployee(props) {

    return (
        <div>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Employee Number</th>
                        <th>Customer Name</th>
                        <th>VIN #</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {props.sales.map(sale =>{
                        return (
                            <tr key={ sale.id }>
                                <td>{ sale.sales_person.name }</td>
                                <td>{ sale.sales_person.employee_number }</td>
                                <td>{ sale.customer.name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>${ sale.purchase_price }.00</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
)

}

export default SalesByEmployee;
