import React from 'react';

class NewCustomerForm extends React.Component {

    state = {
      name: '',
      address: '',
      phone_number: ''
    };

    handleChange = (e) => {
      const value = e.target.value;
      const name = e.target.name;
      this.setState({[name]: value});
    };

    handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...this.state};
        const salesUrl = 'http://localhost:8090/api/sales/customer/';
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(salesUrl, fetchConfig);
        if (response.ok) {

          const cleared = {
            name: '',
            address: '',
            phone_number: ''
          };
          this.setState(cleared);
        }
      }

    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create Customer</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange= {this.handleChange} value={this.state.name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                  <label htmlFor="style">Customer Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleChange} value={this.state.address} placeholder="Address" required type="text" name="address" id="address" className="form-control"/>
                  <label htmlFor="style">Customer Address</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleChange} value={this.state.phone_number} placeholder="PhoneNumber" required type="text" name="phone_number" id="phone_number" className="form-control"/>
                  <label htmlFor="style">Phone Number</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default NewCustomerForm;
