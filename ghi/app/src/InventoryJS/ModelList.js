//Luke


import React from 'react';


class ModelList extends React.Component {
    constructor(){
        super()
        this.state = {models: []};
        }

    async componentDidMount(){
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({models: data.models});
        }
    }
    render() {
    return (
      <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {this.state.models.map(mod => {
            return (
              <tr key={ mod.id }>
                <td>{ mod.name }</td>
                <td>{ mod.manufacturer.name }</td>
                <td><img src= { mod.picture_url } height= "90px" width="180px" alt="model" ></img></td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
    }
}
export default ModelList;
