import React from 'react';
//Gio
class NewAutoForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            color: '',
            year: '',
            vin: '',
            model_id: '',
            models: []
        };

        this.handleColorChange = this.handleColorChange.bind(this);
        this.handleYearChange = this.handleYearChange.bind(this);
        this.handleVinChange = this.handleVinChange.bind(this)
        this.handleModelChange = this.handleModelChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleColorChange(event) {
        const value = event.target.value;
        this.setState({color: value})
    }
    handleYearChange(event) {
        const value = event.target.value;
        this.setState({year: value})
    }
    handleVinChange(event) {
        const value = event.target.value;
        this.setState({vin: value})
    }
    handleModelChange(event) {
        const value = event.target.value;
        this.setState({model_id: value})
    }


    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.models

        const Url = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(Url, fetchConfig);
        if (response.ok) {

          const cleared = {
            color: '',
            year: '',
            vin:'',
            model_id: ''
          };
          this.setState(cleared);
        }
      }
      async componentDidMount(){
        const url = "http://localhost:8100/api/models/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({models: data.models})
        }
      }

    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Add New Automobile</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange= {this.handleColorChange} value={this.state.color} placeholder="Color" required type="text" name="Color" id="Color" className="form-control"/>
                  <label htmlFor="style">Color</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleYearChange} value={this.state.year} placeholder="Year" required type="text" name="Year" id="Year" className="form-control"/>
                  <label htmlFor="style">Year</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handleVinChange} value={this.state.vin} placeholder="Vin" required type="text" name="Vin" id="Vin" className="form-control"/>
                  <label htmlFor="style">Vin</label>
                </div>
                <select onChange= {this.handleModelChange} value={this.state.model_id} required name="Models" id="Models" className="form-select">
                    <option value="">Choose a Model</option>
                    {this.state.models.map(mod => {
                        return (
                        <option key= {mod.id} value={mod.id}>
                            {mod.name}
                        </option>
                        );
                    })}
                  </select>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default NewAutoForm;
