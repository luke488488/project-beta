import React from 'react';

//Gio
class ManufacturerList extends React.Component {
    constructor(){
        super()
        this.state = {manufacturers: []};
        }

    async componentDidMount(){
        const url = "http://localhost:8100/api/manufacturers/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({manufacturers: data.manufacturers});
        }
    }
    render() {
    return (
      <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturers</th>
          </tr>
        </thead>
        <tbody>
          {this.state.manufacturers.map(man => {
            return (
              <tr key={ man.id }>
                <td>{ man.name }</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
    }
}
export default ManufacturerList;
