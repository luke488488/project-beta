import React from 'react';
//Luke
class VehicleModelForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            picture_url: '',
            manufacturer_id: '',
            manufacturers: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePictureChange = this.handlePictureChange.bind(this);
        this.handleManufacturerChange = this.handleManufacturerChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }
    handlePictureChange(event) {
        const value = event.target.value;
        this.setState({picture_url: value})
    }
    handleManufacturerChange(event) {
        const value = event.target.value;
        this.setState({manufacturer_id: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.manufacturers

        const Url = 'http://localhost:8100/api/models/';
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(Url, fetchConfig);
        if (response.ok) {

          const cleared = {
            name: '',
            picture_url: '',
            manufacturer: ''
          };
          this.setState(cleared);
        }
      }
      async componentDidMount(){
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({manufacturers: data.manufacturers})
        }
      }

    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create a new Model</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange= {this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="Name" id="Name" className="form-control"/>
                  <label htmlFor="style">Name</label>
                </div>
                <div className="form-floating mb-3">
                  <input onChange= {this.handlePictureChange} value={this.state.picture_url} placeholder="Name" required type="text" name="Name" id="Name" className="form-control"/>
                  <label htmlFor="style">Picture</label>
                </div>
                <select onChange= {this.handleManufacturerChange} value={this.state.manufacturer_id} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                    <option value="">Choose a Manufacturer</option>
                    {this.state.manufacturers.map(man => {
                        return (
                        <option key= {man.name} value={man.id}>
                            {man.name}
                        </option>
                        );
                    })}
                  </select>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default VehicleModelForm;
