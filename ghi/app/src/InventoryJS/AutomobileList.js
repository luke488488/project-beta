import React from 'react';

//Gio
class AutoList extends React.Component {
    constructor(){
        super()
        this.state = {automobiles: []};
        }

    async componentDidMount(){
        const url = "http://localhost:8100/api/automobiles/";
        const response = await fetch(url);
        if (response.ok){
            const data = await response.json();
            this.setState({automobiles: data.autos});
        }
    }
    render() {
    return (
      <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Year</th>
            <th>Color</th>
            <th>Vin</th>
            <th>Picture</th>
          </tr>
        </thead>
        <tbody>
          {this.state.automobiles.map(auto => {
            return (
              <tr key={ auto.id }>
                <td>{ auto.model.manufacturer.name }</td>
                <td>{ auto.model.name }</td>
                <td>{ auto.year }</td>
                <td>{ auto.color }</td>
                <td>{ auto.vin }</td>
                <td><img src= { auto.model.picture_url } height= "90px" width="180px" alt="model" ></img></td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </div>
    );
    }
}
export default AutoList;
