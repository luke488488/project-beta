import React from 'react';

//Luke
class ManufacturerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};

        const Url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
          method: "POST",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(Url, fetchConfig);
        if (response.ok) {

          const cleared = {
            name: '',
            employee_number: '',
          };
          this.setState(cleared);
        }
      }

    render() {
      return (
        <div className="row">
          <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
              <h1>Create Manufacturer</h1>
              <form onSubmit={this.handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                  <input onChange= {this.handleNameChange} value={this.state.name} placeholder="Name" required type="text" name="Name" id="Name" className="form-control"/>
                  <label htmlFor="style">Name</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      );
    }
  }

export default ManufacturerForm;
