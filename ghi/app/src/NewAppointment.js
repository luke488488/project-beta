import React, { useState, useEffect } from 'react'
const ApptForm = ({getAppts}) => {
    const[owner, setOwner] = useState('');
    const[date, setDate] = useState('');
    const[reason, setReason] = useState('');
    const[tech, setTech] = useState('');
    const[techs, setTechs] = useState([]);
    const[vin, setVin] = useState('');

    useEffect(() => {
        const techUrl = "http://localhost:8080/api/technicians/"
        fetch(techUrl)
            .then(response => response.json())
            .then(data => setTechs(data.techs))
            .catch(e => console.error('error: ', e))
    }, [])

    const submit = (event) => {
        event.preventDefault();
        const newAppt = {
            'owner': owner,
            'date': date,
            'reason': reason,
            'tech': tech,
            'vin': vin,
        }
        const apptsUrl = "http://localhost:8080/api/appointments/"
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newAppt),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(apptsUrl, fetchConfig)
            .then(response => {
                if (response.ok){
                    return getAppts()
                }
            })
            .then(() => {
                setOwner('');
                setDate('');
                setReason('');
                setTech('');
                setVin('')
            })
            .catch(e => console.error('error: ', e))
    }
    const handleOwnerChange = (event) => {
        const value = event.target.value;
        setOwner(value);
    }
    const handleDateChange = (event) => {
        const value = event.target.value;
        setDate(value);
    }
    const handleReasonChange = (event) => {
        const value = event.target.value;
        setReason(value);
    }
    const handleTechChange = (event) => {
        const value = event.target.value;
        setTech(value);
    }
    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Book Appointment</h1>
            <form onSubmit={submit} id="create-shoe-form">
              <div className="form-floating mb-3">
                <input onChange={handleOwnerChange} value={owner} placeholder="owner" required type="text" name="owner" id="owner" className="form-control"/>
                <label htmlFor="owner">Owner</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDateChange} value={date} placeholder="date" required type="datetime-local" name="date" id="date" className="form-control"/>
                <label htmlFor="date">date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleVinChange} value={vin} placeholder="vin" required type="number" name="vin" id="vin" className="form-control"/>
                <label htmlFor="vin">Vin</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleReasonChange} value={reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control"/>
                <label htmlFor="reason">Reason</label>
              </div>
              <div className="mb-3">
                <select onChange={handleTechChange} value={tech} required id="tech" name="tech" className="form-select">
                  <option value="">Choose Technician</option>
                {techs.map(tech => {
                    return (
                        <option key={tech.employee_number} value={tech.employee_number}>
                            {tech.name}
                        </option>
                    )
                })}
                </select>
              </div>

              <button className="btn btn-warning">Add</button>
            </form>
          </div>
        </div>
      </div>
    )
}


export default ApptForm;
