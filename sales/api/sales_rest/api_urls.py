from django.urls import path

from.views import api_sales, api_sales_person, api_customer

urlpatterns = [
    path("sales/", api_sales, name="api_sales"),
    path("sales/salesperson/", api_sales_person, name="api_sales_people"),
    path("sales/customer/", api_customer, name="api_customers"),
    path("sales/salesperson/<int:employee_number>/", api_sales, name="api_sales_detail"),
]
