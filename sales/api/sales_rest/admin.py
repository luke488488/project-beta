from django.contrib import admin
from .models import SalesPerson, Customer, SalesLog, AutomobileVO
# Register your models here.


@admin.register(SalesPerson)
class SalesPerson (admin.ModelAdmin):
    pass

@admin.register(SalesLog)
class SalesLog (admin.ModelAdmin):
    pass

@admin.register(Customer)
class Customer (admin.ModelAdmin):
    pass

@admin.register(AutomobileVO)
class AutomobileVO (admin.ModelAdmin):
    pass
