import json
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from .models import SalesLog, SalesPerson, Customer, AutomobileVO
from .models import AutomobileVO, SalesLog, SalesPerson, Customer


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "import_href"]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "id",
        "name",
        "employee_number",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "id",
        "name",
        "address",
        "phone_number",
    ]

class SalesLogEncoder(ModelEncoder):
    model = SalesLog
    properties = [
        "id",
        "sales_person",
        "automobile",
        "purchase_price",
        "customer"
    ]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": CustomerEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_sales_person(request):
    if request.method == "GET":
        sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_person": sales_person},
            encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create sales person"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customer = Customer.objects.all()
        return JsonResponse(
            {"customer": customer},
            encoder=CustomerEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False,
            )
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response



@require_http_methods(["GET", "POST"])
def api_sales(request, employee_number=None):
    if request.method == "GET":
        if employee_number != None:
            sales_person = SalesPerson.objects.get(employee_number=employee_number)
            sales = SalesLog.objects.filter(sales_person=sales_person)
        else:
            sales = SalesLog.objects.all()
        return JsonResponse(
            {"sales": sales},
            encoder=SalesLogEncoder,
        )
    else:
        try:
            content = json.loads(request.body)

            sale_id = content["sales_person"]
            seller_id = SalesPerson.objects.get(employee_number=sale_id)
            content["sales_person"] = seller_id
        except:
            response = JsonResponse(
                {"message": "Could not create sales person"}
            )
            response.status_code = 400
            return response
        try:
            vin = content["automobile"]
            vehicle = AutomobileVO.objects.get(vin=vin)
            content["automobile"] = vehicle
        except:
            response = JsonResponse(
                {"message": "Could not create automobile"}
            )
            response.status_code = 400
            return response
        try:
            customer_id = content["customer"]
            buyer_id = Customer.objects.get(id=customer_id)
            content["customer"] = buyer_id
        except:
            response = JsonResponse(
                {"message": "Could not create customer"}
            )
            response.status_code = 400
            return response


        sale = SalesLog.objects.create(**content)
        print("sale",sale)
        return JsonResponse(
            sale,
            encoder=SalesLogEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_sale(request, vin):
    if request.method == "GET":
        try:
            sale = SalesLog.objects.get(vin=vin)
            return JsonResponse(
                sale,
                encoder=SalesLogEncoder,
                safe=False
            )
        except SalesLog.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            sale = SalesLog.objects.get(vin=vin)
            sale.delete()
            return JsonResponse(
                sale,
                encoder=SalesLogEncoder,
                safe=False,
            )
        except SalesLog.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            sale = SalesLog.objects.get(vin=vin)

            props = ["purchase_price", "customer", "sales_person"]
            for prop in props:
                if prop in content:
                    setattr(sale, prop, content[prop])
            sale.save()
            return JsonResponse(
                sale,
                encoder=SalesLogEncoder,
                safe=False,
            )
        except SalesLog.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
