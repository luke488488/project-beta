# CarCar

The CarCar project generates and manages inventory for a vehicle dealership's sales, service center, and parts department. This project's design aims to develop RESTful API calls to the three distinct microservices of inventory, sales, and service.

React is utilized for the application's front end, Django for its back end, and PostgreSQL for its database.

KEEP IN MIND, The Sales Record model would require the establishment of an Automobile from the Inventory service initially to assure appropriate creation,


Team:

* Luke - Service
* Gio - Sales


## How to run this application - Gio
* Before Running this Application please make sure you have VS CODE, and Docker Installed !!

* Okay now that you have all of that install please open up your terminal.

#### Cloning the Repository - Gio
* In a terminal, change directories to one that you can clone this project into.
* Type this command into your terminal: git clone https://gitlab.com/luke488488/project-beta.git

* CD into this project directory.
* Type "Code ." to open vs code

#### Starting the application with Docker - Gio
* After opening up VS CODE switch back to the project's directory in your terminal, type the following commands once and hit enter after each one:

* docker volume create beta-data
* docker-compose build
* docker-compose up

## Application Diagram
Diagram using ExcaliDraw > https://imgur.com/a/5WGxcIs
## Service microservice

    I made the technician and appointment model with the requred base values then added a foreign key to access the technician model in the appointment model. I also made a VehicleVO model to grab the vin from the vehicle model in the inventory api. I then made the poller create VehicleVO objects for me to use by polling the inventory api and copying the  vin values from inventory Vehicle objects. The appointments list was just mapping all the appointment objects and displaying the desired values with some if statements for checking if the vin in the appointment matched the vin in one of the vehiclevo objects which will add the vip pug to that item in the list and another if to check if the appointment is finished or not. The technician form is just a standard form crating a new technician object. The appointment form is standard except it pulls from the list of technicians to give a dropdown of technicians to chose from. any changes that would effect the appointment list such as adding, deleting or finishing calls the getAppts function to update the list in real time.

## Sales microservice - Gio

In this project I created models for the AutomobileVO, SalesPerson, and Customer components of the Sales Microservice. A value object called Automobile had to be built in order to access the data for Automobiles, which was kept in the Inventory microservice. Then, to collect this information, I developed a function named "poll" in which you can find in Sales>api>poll>poller.py.

In the Sales Microservice's views.py file, I have implemented view functions for each model to ensure that the back-end data is created and stored properly. I then carried out my RESTful API queries using Insomnia to confirm that this was correct.

ALWAYS REMEMBER, To ensure proper creation, the Sales Record model would first require the creation of an Automobile from the Inventory service.

## Sales Microservice: - Gio

| Feature          | Method          | URL          |
|:-----------------|:----------------|:-------------|
|Show Sales Performance| GET |http://localhost:3000/sales/performance|
|Create Sales Form| POST |http://localhost:3000/sales/new/|
|Create a Customer| POST |http://localhost:8090/api/customers/|
|Create a Sales Man| POST |http://localhost:3000/sales/employee|
